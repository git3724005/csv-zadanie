import csv
import sys

def read_csv(filename):
    data = []
    with open(filename, newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(row)
    return data

def apply_changes(data, changes):
    for change in changes:
        x, y, value = change.split(',')
        data[int(y)][int(x)] = value

def write_csv(filename, data):
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(data)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        sys.exit(1)

    input_file = sys.argv[1]
    output_file = sys.argv[2]
    changes = sys.argv[3:]

    try:
        data = read_csv(input_file)
        apply_changes(data, changes)
        write_csv(output_file, data)
        print(f"\nModified data saved to {output_file}")
    except Exception as e:
        print(f"Error: {e}")
